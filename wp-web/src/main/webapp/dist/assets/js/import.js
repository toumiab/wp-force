///Temporary function for Web components polyfill support
function import_partial(base, from, to) {
    
     window.addEventListener('HTMLImportsLoaded', function() {
        var link = document.querySelector(base);
        var content = link.import.querySelector(from);
       // document.body.appendChild(document.importNode(content, true));
          
        var refNode = document.getElementById(to);
        refNode.appendChild(document.importNode(content, true));
      });
}