package com.wp.rest.conf;

import org.glassfish.jersey.message.GZipEncoder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.EncodingFilter;

public class RestJaxRsApplication extends ResourceConfig {

		public RestJaxRsApplication(){
			packages("com.wp.rest.controller");
			EncodingFilter.enableFor(this, GZipEncoder.class);
		}
}
