package com.wp.rest.conf;

/**
 * 
 */

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;


/**
 * @author atoumi
 *
 */

@SuppressWarnings("rawtypes")
@Provider
public class RuntimeMapper implements ExceptionMapper{
	
	static final Logger LOGGER = Logger.getLogger(RuntimeMapper.class);
	 
	public Response toResponse(Throwable e) {
	 
		return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
	}
	
	public Response toResponse(String e) {
		 
		return Response.status(Status.BAD_REQUEST).entity(e).build();
	}
}
