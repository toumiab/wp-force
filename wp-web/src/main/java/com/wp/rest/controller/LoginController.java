/**
 * 
 */
package com.wp.rest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.wp.commun.representation.GroupeRepresentation;
import org.wp.commun.representation.UserAccountRepresentation;
import org.wp.commun.representation.WPException;
import org.wp.core.services.GroupeServices;
import org.wp.core.services.UserServices;

import com.wp.rest.conf.RuntimeMapper;

/**
 * @author atoumi
 *
 */
@Component
@Path("/login")
public class LoginController {

	@Autowired
	private GroupeServices groupeService;
	
	@Autowired
	private UserServices userServiuce;	
	
	static final Logger LOGGER = Logger.getLogger(LoginController.class);
	
	private RuntimeMapper exception;	
	
	@GET
    @Path("/connexion/{login}/{motPass}")	
    @Produces({MediaType.APPLICATION_JSON})
	public Response getMessage(@PathParam("login") String login, @PathParam("motPass") String motPass){
    	String objet = "test11"+login+"---"+motPass;
    	LOGGER.debug(objet);
    	UserAccountRepresentation userRep = this.userServiuce.findUser(login, motPass);
		return Response.status(200).entity(userRep).build();
	}
    
    @GET
    @Path("/groupes")
    public Response getGroupes(){
    	List<GroupeRepresentation> lists =  new ArrayList<GroupeRepresentation>();
    	lists = groupeService.getAllGroupe();
    	if(null != lists){
    		LOGGER.debug("----------list size-------"+lists.size());
    	}    	
    	return Response.status(200).entity(lists).build();
    }
    
    @POST
    @Path("/createGroupe")
    public Response createGroupe(GroupeRepresentation groupeRep) {
    	String result = null;
    	try {
    		result = this.groupeService.createGroupe(groupeRep);
		}catch (WPException e) {
			return exception.toResponse(e);
		} 
    	return Response.status(200).entity(result).build();
    }
    
    @POST
    @Path("/updateGroupe")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.TEXT_HTML})
    public Response updateGroupe(@PathParam("id") long id, GroupeRepresentation groupeRep){
    	try {
			this.groupeService.updateGroupe(groupeRep);
		} catch (WPException e) {
			return exception.toResponse(e);
		}
    	return Response.status(Response.Status.OK).entity("ok").build();
    	
    }    
    
    @DELETE
    @Path("/deleteGroupe/{id}")
    @Produces({ MediaType.TEXT_HTML})
    public Response deletePodcastById(@PathParam("id") int id){
    	try {
			this.groupeService.deleteGroupe(id);
		} catch (WPException e) {
			return exception.toResponse(e);
		}
    	return Response.status(Response.Status.NO_CONTENT).entity("ok").build();
    }    
}
