/**
 * 
 */
package org.wp.commun.representation;

/**
 * @author atoumi
 *
 */
public class GroupeRepresentation {
	
	private int idGroup;
	
	private String description;
	
	private String groupName;
	
	private int superGroup;

	/**
	 * @return the idGroup
	 */
	public int getIdGroup() {
		return idGroup;
	}

	/**
	 * @param idGroup the idGroup to set
	 */
	public void setIdGroup(int idGroup) {
		this.idGroup = idGroup;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the superGroup
	 */
	public int getSuperGroup() {
		return superGroup;
	}

	/**
	 * @param superGroup the superGroup to set
	 */
	public void setSuperGroup(int superGroup) {
		this.superGroup = superGroup;
	}
	
	

}
