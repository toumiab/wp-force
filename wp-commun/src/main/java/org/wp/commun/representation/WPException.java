/**
 * 
 */
package org.wp.commun.representation;

/**
 * @author atoumi
 *
 */
public class WPException extends Exception {

	
	/** UID. */
	private static final long serialVersionUID = -3719092326265950285L;
	
	
	/** object that causes this exception. */
	private final Object causeObject;

	/** attribute of object that causes this exception. */
	private final String causeAttribute;

	/**
	 * Instantiates a new liverpr business exception.
	 * 
	 * @param msg the msg
	 * @param cause the cause
	 */
	public WPException(String msg, Throwable cause) {
		this(msg, cause, null, null);
	}

	/**
	 * Instantiates a new liverpr business exception.
	 * 
	 * @param msg the msg
	 */
	public WPException(String msg) {
		this(msg, null, null);
	}

	/**
	 * Instantiates a new liverpr business exception.
	 * 
	 * @param cause the cause
	 */
	public WPException(Throwable cause) {
		super(cause);
		this.causeObject = null;
		this.causeAttribute = null;
	}

	/**
	 * Instantiates a new liverpr business exception.
	 * 
	 * @param msg message for this exception
	 * @param cause the cause root cause
	 * @param causeObj object that causes this exception
	 * @param causeAttr attribute of object that causes this exception
	 */
	public WPException(String msg, Throwable cause, Object causeObj, String causeAttr) {
		super(msg, cause);
		this.causeObject = causeObj;
		this.causeAttribute = causeAttr;
	}

	/**
	 * Instantiates a new liverpr business exception.
	 * 
	 * @param msg message for this exception
	 * @param causeObj object that causes this exception
	 * @param causeAttr attribute of object that causes this exception
	 */
	public WPException(String msg, Object causeObj, String causeAttr) {
		super(msg);
		this.causeObject = causeObj;
		this.causeAttribute = causeAttr;
	}

	/**
	 * Getter causeObject.
	 *
	 * @return the causeObject
	 */
	public Object getCauseObject() {
		return causeObject;
	}

	/**
	 * Getter causeAttribute.
	 *
	 * @return the causeAttribute
	 */
	public String getCauseAttribute() {
		return causeAttribute;
	}
}
