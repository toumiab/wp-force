/**
 * 
 */
package org.wp.commun.representation;

import java.sql.Timestamp;


/**
 * @author Abdennasser
 *
 */
public class UserAccountRepresentation {

	private int id;

	private String email;

	private byte emailConfirmed;

	private String firstName;

	private int lockoutEnable;

	private int lockoutEndDateUtc;

	private String name;

	private String organisation;

	private String passwordHash;

	private int phoneNumber;

	private int phoneNumberConfirmed;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the emailConfirmed
	 */
	public byte getEmailConfirmed() {
		return emailConfirmed;
	}

	/**
	 * @param emailConfirmed the emailConfirmed to set
	 */
	public void setEmailConfirmed(byte emailConfirmed) {
		this.emailConfirmed = emailConfirmed;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lockoutEnable
	 */
	public int getLockoutEnable() {
		return lockoutEnable;
	}

	/**
	 * @param lockoutEnable the lockoutEnable to set
	 */
	public void setLockoutEnable(int lockoutEnable) {
		this.lockoutEnable = lockoutEnable;
	}

	/**
	 * @return the lockoutEndDateUtc
	 */
	public int getLockoutEndDateUtc() {
		return lockoutEndDateUtc;
	}

	/**
	 * @param lockoutEndDateUtc the lockoutEndDateUtc to set
	 */
	public void setLockoutEndDateUtc(int lockoutEndDateUtc) {
		this.lockoutEndDateUtc = lockoutEndDateUtc;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the organisation
	 */
	public String getOrganisation() {
		return organisation;
	}

	/**
	 * @param organisation the organisation to set
	 */
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	/**
	 * @return the passwordHash
	 */
	public String getPasswordHash() {
		return passwordHash;
	}

	/**
	 * @param passwordHash the passwordHash to set
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	/**
	 * @return the phoneNumber
	 */
	public int getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the phoneNumberConfirmed
	 */
	public int getPhoneNumberConfirmed() {
		return phoneNumberConfirmed;
	}

	/**
	 * @param phoneNumberConfirmed the phoneNumberConfirmed to set
	 */
	public void setPhoneNumberConfirmed(int phoneNumberConfirmed) {
		this.phoneNumberConfirmed = phoneNumberConfirmed;
	}
	
	
	
	
}
