package org.wp.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the role_user database table.
 * 
 */
@Entity
@Table(name="role_user")
@NamedQuery(name="RoleUser.findAll", query="SELECT r FROM RoleUser r")
public class RoleUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ROLE_USER_ROLE_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.AUTO, generator="ROLE_USER_ROLE_GENERATOR")
	private String role;

	private int column;

	private String description;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="Role", referencedColumnName="Type_userRole")
	private User user;

	//bi-directional many-to-one association to Permission
	@OneToMany(mappedBy="roleUser")
	private List<Permission> permissions;

	public RoleUser() {
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getColumn() {
		return this.column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Permission> getPermissions() {
		return this.permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	public Permission addPermission(Permission permission) {
		getPermissions().add(permission);
		permission.setRoleUser(this);

		return permission;
	}

	public Permission removePermission(Permission permission) {
		getPermissions().remove(permission);
		permission.setRoleUser(null);

		return permission;
	}

}