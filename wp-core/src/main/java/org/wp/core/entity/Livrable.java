package org.wp.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the livrable database table.
 * 
 */
@Entity
@NamedQuery(name="Livrable.findAll", query="SELECT l FROM Livrable l")
public class Livrable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="LIVRABLE_IDLIVRABLE_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.AUTO, generator="LIVRABLE_IDLIVRABLE_GENERATOR")
	@Column(name="id_livrable")
	private int idLivrable;

	@Column(name="budget_prevu")
	private int budgetPrevu;

	@Column(name="budget_reel")
	private int budgetReel;

	@Temporal(TemporalType.DATE)
	@Column(name="date_debut_fin")
	private Date dateDebutFin;

	@Temporal(TemporalType.DATE)
	@Column(name="date_debut_prev")
	private Date dateDebutPrev;

	@Column(name="nom_livrable")
	private String nomLivrable;

	//bi-directional many-to-one association to Uniteoeuvre
	@OneToMany(mappedBy="livrable")
	private List<Uniteoeuvre> uniteoeuvres;

	//bi-directional many-to-one association to Budget
	@OneToMany(mappedBy="livrable")
	private List<Budget> budgets;

	//bi-directional many-to-one association to Workpage
	@ManyToOne
	@JoinColumn(name="workpageid_wp")
	private Workpage workpage;

	public Livrable() {
	}

	public int getIdLivrable() {
		return this.idLivrable;
	}

	public void setIdLivrable(int idLivrable) {
		this.idLivrable = idLivrable;
	}

	public int getBudgetPrevu() {
		return this.budgetPrevu;
	}

	public void setBudgetPrevu(int budgetPrevu) {
		this.budgetPrevu = budgetPrevu;
	}

	public int getBudgetReel() {
		return this.budgetReel;
	}

	public void setBudgetReel(int budgetReel) {
		this.budgetReel = budgetReel;
	}

	public Date getDateDebutFin() {
		return this.dateDebutFin;
	}

	public void setDateDebutFin(Date dateDebutFin) {
		this.dateDebutFin = dateDebutFin;
	}

	public Date getDateDebutPrev() {
		return this.dateDebutPrev;
	}

	public void setDateDebutPrev(Date dateDebutPrev) {
		this.dateDebutPrev = dateDebutPrev;
	}

	public String getNomLivrable() {
		return this.nomLivrable;
	}

	public void setNomLivrable(String nomLivrable) {
		this.nomLivrable = nomLivrable;
	}

	public List<Uniteoeuvre> getUniteoeuvres() {
		return this.uniteoeuvres;
	}

	public void setUniteoeuvres(List<Uniteoeuvre> uniteoeuvres) {
		this.uniteoeuvres = uniteoeuvres;
	}

	public Uniteoeuvre addUniteoeuvre(Uniteoeuvre uniteoeuvre) {
		getUniteoeuvres().add(uniteoeuvre);
		uniteoeuvre.setLivrable(this);

		return uniteoeuvre;
	}

	public Uniteoeuvre removeUniteoeuvre(Uniteoeuvre uniteoeuvre) {
		getUniteoeuvres().remove(uniteoeuvre);
		uniteoeuvre.setLivrable(null);

		return uniteoeuvre;
	}

	public List<Budget> getBudgets() {
		return this.budgets;
	}

	public void setBudgets(List<Budget> budgets) {
		this.budgets = budgets;
	}

	public Budget addBudget(Budget budget) {
		getBudgets().add(budget);
		budget.setLivrable(this);

		return budget;
	}

	public Budget removeBudget(Budget budget) {
		getBudgets().remove(budget);
		budget.setLivrable(null);

		return budget;
	}

	public Workpage getWorkpage() {
		return this.workpage;
	}

	public void setWorkpage(Workpage workpage) {
		this.workpage = workpage;
	}

}