package org.wp.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the workpage database table.
 * 
 */
@Entity
@NamedQuery(name="Workpage.findAll", query="SELECT w FROM Workpage w")
public class Workpage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="WORKPAGE_IDWP_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.AUTO, generator="WORKPAGE_IDWP_GENERATOR")
	@Column(name="id_wp")
	private int idWp;

	@Column(name="budget_prevu")
	private int budgetPrevu;

	@Column(name="budget_reel")
	private int budgetReel;

	@Column(name="budgetid_budget")
	private int budgetidBudget;

	@Temporal(TemporalType.DATE)
	@Column(name="date_debut_prev")
	private Date dateDebutPrev;

	@Temporal(TemporalType.DATE)
	@Column(name="date_fin_prev")
	private Date dateFinPrev;

	@Column(name="nom_wp")
	private String nomWp;

	@Column(name="nomb_livrable")
	private int nombLivrable;

	//bi-directional many-to-many association to User
	@ManyToMany(mappedBy="workpages")
	private List<User> users;

	//bi-directional many-to-one association to Budget
	@OneToMany(mappedBy="workpage")
	private List<Budget> budgets;

	//bi-directional many-to-one association to Livrable
	@OneToMany(mappedBy="workpage")
	private List<Livrable> livrables;

	public Workpage() {
	}

	public int getIdWp() {
		return this.idWp;
	}

	public void setIdWp(int idWp) {
		this.idWp = idWp;
	}

	public int getBudgetPrevu() {
		return this.budgetPrevu;
	}

	public void setBudgetPrevu(int budgetPrevu) {
		this.budgetPrevu = budgetPrevu;
	}

	public int getBudgetReel() {
		return this.budgetReel;
	}

	public void setBudgetReel(int budgetReel) {
		this.budgetReel = budgetReel;
	}

	public int getBudgetidBudget() {
		return this.budgetidBudget;
	}

	public void setBudgetidBudget(int budgetidBudget) {
		this.budgetidBudget = budgetidBudget;
	}

	public Date getDateDebutPrev() {
		return this.dateDebutPrev;
	}

	public void setDateDebutPrev(Date dateDebutPrev) {
		this.dateDebutPrev = dateDebutPrev;
	}

	public Date getDateFinPrev() {
		return this.dateFinPrev;
	}

	public void setDateFinPrev(Date dateFinPrev) {
		this.dateFinPrev = dateFinPrev;
	}

	public String getNomWp() {
		return this.nomWp;
	}

	public void setNomWp(String nomWp) {
		this.nomWp = nomWp;
	}

	public int getNombLivrable() {
		return this.nombLivrable;
	}

	public void setNombLivrable(int nombLivrable) {
		this.nombLivrable = nombLivrable;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Budget> getBudgets() {
		return this.budgets;
	}

	public void setBudgets(List<Budget> budgets) {
		this.budgets = budgets;
	}

	public Budget addBudget(Budget budget) {
		getBudgets().add(budget);
		budget.setWorkpage(this);

		return budget;
	}

	public Budget removeBudget(Budget budget) {
		getBudgets().remove(budget);
		budget.setWorkpage(null);

		return budget;
	}

	public List<Livrable> getLivrables() {
		return this.livrables;
	}

	public void setLivrables(List<Livrable> livrables) {
		this.livrables = livrables;
	}

	public Livrable addLivrable(Livrable livrable) {
		getLivrables().add(livrable);
		livrable.setWorkpage(this);

		return livrable;
	}

	public Livrable removeLivrable(Livrable livrable) {
		getLivrables().remove(livrable);
		livrable.setWorkpage(null);

		return livrable;
	}

}