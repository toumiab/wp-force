package org.wp.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the action_user database table.
 * 
 */
@Entity
@Table(name="action_user")
@NamedQuery(name="ActionUser.findAll", query="SELECT a FROM ActionUser a")
public class ActionUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACTION_USER_IDACTION_GENERATOR", sequenceName="SEQUENCE_USER")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="ACTION_USER_IDACTION_GENERATOR")
	@Column(name="id_action")
	private int idAction;

	@Temporal(TemporalType.DATE)
	@Column(name="date_action")
	private Date dateAction;

	@Column(name="nom_action")
	private String nomAction;

	@Column(name="nom_user")
	private String nomUser;

	@Column(name="role_user")
	private String roleUser;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="userId")
	private User user;

	public ActionUser() {
	}

	public int getIdAction() {
		return this.idAction;
	}

	public void setIdAction(int idAction) {
		this.idAction = idAction;
	}

	public Date getDateAction() {
		return this.dateAction;
	}

	public void setDateAction(Date dateAction) {
		this.dateAction = dateAction;
	}

	public String getNomAction() {
		return this.nomAction;
	}

	public void setNomAction(String nomAction) {
		this.nomAction = nomAction;
	}

	public String getNomUser() {
		return this.nomUser;
	}

	public void setNomUser(String nomUser) {
		this.nomUser = nomUser;
	}

	public String getRoleUser() {
		return this.roleUser;
	}

	public void setRoleUser(String roleUser) {
		this.roleUser = roleUser;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}