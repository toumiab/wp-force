package org.wp.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the fournisseur database table.
 * 
 */
@Entity
@NamedQuery(name="Fournisseur.findAll", query="SELECT f FROM Fournisseur f")
public class Fournisseur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FOURNISSEUR_IDFOURNISSEUR_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.AUTO, generator="FOURNISSEUR_IDFOURNISSEUR_GENERATOR")
	@Column(name="id_fournisseur")
	private int idFournisseur;

	@Column(name="domaine_activite")
	private String domaineActivite;

	@Column(name="nom_fournisseur")
	private String nomFournisseur;

	//bi-directional many-to-one association to ComiteAvancement
	@OneToMany(mappedBy="fournisseur")
	private List<ComiteAvancement> comiteAvancements;

	public Fournisseur() {
	}

	public int getIdFournisseur() {
		return this.idFournisseur;
	}

	public void setIdFournisseur(int idFournisseur) {
		this.idFournisseur = idFournisseur;
	}

	public String getDomaineActivite() {
		return this.domaineActivite;
	}

	public void setDomaineActivite(String domaineActivite) {
		this.domaineActivite = domaineActivite;
	}

	public String getNomFournisseur() {
		return this.nomFournisseur;
	}

	public void setNomFournisseur(String nomFournisseur) {
		this.nomFournisseur = nomFournisseur;
	}

	public List<ComiteAvancement> getComiteAvancements() {
		return this.comiteAvancements;
	}

	public void setComiteAvancements(List<ComiteAvancement> comiteAvancements) {
		this.comiteAvancements = comiteAvancements;
	}

	public ComiteAvancement addComiteAvancement(ComiteAvancement comiteAvancement) {
		getComiteAvancements().add(comiteAvancement);
		comiteAvancement.setFournisseur(this);

		return comiteAvancement;
	}

	public ComiteAvancement removeComiteAvancement(ComiteAvancement comiteAvancement) {
		getComiteAvancements().remove(comiteAvancement);
		comiteAvancement.setFournisseur(null);

		return comiteAvancement;
	}

}