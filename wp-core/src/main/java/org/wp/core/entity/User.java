package org.wp.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="USER_ID_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.AUTO, generator="USER_ID_GENERATOR")
	private int id;

	private String email;

	private byte emailConfirmed;

	private String firstName;

	private int lockoutEnable;

	private int lockoutEndDateUtc;

	private String name;

	private String organisation;

	private String passwordHash;

	private int phoneNumber;
	
	private int groupeid;

	private int phoneNumberConfirmed;
	
	private String Type_userRole;

	//bi-directional many-to-many association to Workpage
	@ManyToMany
	@JoinTable(
		name="user_workpage"
		, joinColumns={
			@JoinColumn(name="UserId")
			}
		, inverseJoinColumns={
			@JoinColumn(name="workpageid_wp")
			}
		)
	private List<Workpage> workpages;

	//bi-directional many-to-one association to Groupe
	@OneToMany(mappedBy="user")
	private List<Groupe> groupes;

	//bi-directional many-to-one association to RoleUser
	@OneToMany(mappedBy="user")
	private List<RoleUser> roleUsers;

	//bi-directional many-to-one association to ComiteAvancement
	@OneToMany(mappedBy="user")
	private List<ComiteAvancement> comiteAvancements;

	//bi-directional many-to-one association to ActionUser
	@OneToMany(mappedBy="user")
	private List<ActionUser> actionUsers;

	//bi-directional many-to-one association to Groupe
	@ManyToOne
	@JoinColumn(name="groupeid", insertable = false, updatable = false)
	private Groupe groupe;

	public User() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public byte getEmailConfirmed() {
		return this.emailConfirmed;
	}

	public void setEmailConfirmed(byte emailConfirmed) {
		this.emailConfirmed = emailConfirmed;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getLockoutEnable() {
		return this.lockoutEnable;
	}

	public void setLockoutEnable(int lockoutEnable) {
		this.lockoutEnable = lockoutEnable;
	}

	public int getLockoutEndDateUtc() {
		return this.lockoutEndDateUtc;
	}

	public void setLockoutEndDateUtc(int lockoutEndDateUtc) {
		this.lockoutEndDateUtc = lockoutEndDateUtc;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrganisation() {
		return this.organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getPasswordHash() {
		return this.passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public int getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getPhoneNumberConfirmed() {
		return this.phoneNumberConfirmed;
	}

	public void setPhoneNumberConfirmed(int phoneNumberConfirmed) {
		this.phoneNumberConfirmed = phoneNumberConfirmed;
	}

	public List<Workpage> getWorkpages() {
		return this.workpages;
	}

	public void setWorkpages(List<Workpage> workpages) {
		this.workpages = workpages;
	}

	public List<Groupe> getGroupes() {
		return this.groupes;
	}

	public void setGroupes(List<Groupe> groupes) {
		this.groupes = groupes;
	}

	public Groupe addGroupe(Groupe groupe) {
		getGroupes().add(groupe);
		groupe.setUser(this);

		return groupe;
	}

	public Groupe removeGroupe(Groupe groupe) {
		getGroupes().remove(groupe);
		groupe.setUser(null);

		return groupe;
	}

	public List<RoleUser> getRoleUsers() {
		return this.roleUsers;
	}

	public void setRoleUsers(List<RoleUser> roleUsers) {
		this.roleUsers = roleUsers;
	}

	public RoleUser addRoleUser(RoleUser roleUser) {
		getRoleUsers().add(roleUser);
		roleUser.setUser(this);

		return roleUser;
	}

	public RoleUser removeRoleUser(RoleUser roleUser) {
		getRoleUsers().remove(roleUser);
		roleUser.setUser(null);

		return roleUser;
	}

	public List<ComiteAvancement> getComiteAvancements() {
		return this.comiteAvancements;
	}

	public void setComiteAvancements(List<ComiteAvancement> comiteAvancements) {
		this.comiteAvancements = comiteAvancements;
	}

	public ComiteAvancement addComiteAvancement(ComiteAvancement comiteAvancement) {
		getComiteAvancements().add(comiteAvancement);
		comiteAvancement.setUser(this);

		return comiteAvancement;
	}

	public ComiteAvancement removeComiteAvancement(ComiteAvancement comiteAvancement) {
		getComiteAvancements().remove(comiteAvancement);
		comiteAvancement.setUser(null);

		return comiteAvancement;
	}

	public List<ActionUser> getActionUsers() {
		return this.actionUsers;
	}

	public void setActionUsers(List<ActionUser> actionUsers) {
		this.actionUsers = actionUsers;
	}

	public ActionUser addActionUser(ActionUser actionUser) {
		getActionUsers().add(actionUser);
		actionUser.setUser(this);

		return actionUser;
	}

	public ActionUser removeActionUser(ActionUser actionUser) {
		getActionUsers().remove(actionUser);
		actionUser.setUser(null);

		return actionUser;
	}

	public Groupe getGroupe() {
		return this.groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

	/**
	 * @return the groupeid
	 */
	public int getGroupeid() {
		return groupeid;
	}

	/**
	 * @param groupeid the groupeid to set
	 */
	public void setGroupeid(int groupeid) {
		this.groupeid = groupeid;
	}

	/**
	 * @return the type_userRole
	 */
	public String getType_userRole() {
		return Type_userRole;
	}

	/**
	 * @param type_userRole the type_userRole to set
	 */
	public void setType_userRole(String type_userRole) {
		Type_userRole = type_userRole;
	}
	
	

}