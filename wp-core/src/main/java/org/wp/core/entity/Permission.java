package org.wp.core.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the permission database table.
 * 
 */
@Entity
@NamedQuery(name="Permission.findAll", query="SELECT p FROM Permission p")
public class Permission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERMISSION_NOMPERMISSION_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.AUTO, generator="PERMISSION_NOMPERMISSION_GENERATOR")
	@Column(name="nom_permission")
	private String nomPermission;

	@Column(name="description_premission")
	private String descriptionPremission;

	//bi-directional many-to-one association to RoleUser
	@ManyToOne
	@JoinColumn(name="Type_userRole")
	private RoleUser roleUser;

	public Permission() {
	}

	public String getNomPermission() {
		return this.nomPermission;
	}

	public void setNomPermission(String nomPermission) {
		this.nomPermission = nomPermission;
	}

	public String getDescriptionPremission() {
		return this.descriptionPremission;
	}

	public void setDescriptionPremission(String descriptionPremission) {
		this.descriptionPremission = descriptionPremission;
	}

	public RoleUser getRoleUser() {
		return this.roleUser;
	}

	public void setRoleUser(RoleUser roleUser) {
		this.roleUser = roleUser;
	}

}