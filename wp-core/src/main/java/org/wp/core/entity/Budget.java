package org.wp.core.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the budget database table.
 * 
 */
@Entity
@NamedQuery(name="Budget.findAll", query="SELECT b FROM Budget b")
public class Budget implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BUDGET_IDBUDGET_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.AUTO, generator="BUDGET_IDBUDGET_GENERATOR")
	@Column(name="id_budget")
	private int idBudget;

	@Column(name="montant_budget")
	private int montantBudget;

	@Column(name="nom_budget")
	private String nomBudget;

	@Column(name="type_budget")
	private String typeBudget;

	//bi-directional many-to-one association to Uniteoeuvre
	@ManyToOne
	@JoinColumn(name="uniteoeuvreid_uouvre")
	private Uniteoeuvre uniteoeuvre;

	//bi-directional many-to-one association to Livrable
	@ManyToOne
	@JoinColumn(name="livrableid_livrable")
	private Livrable livrable;

	//bi-directional many-to-one association to Workpage
	@ManyToOne
	@JoinColumn(name="workpageid_wp")
	private Workpage workpage;

	public Budget() {
	}

	public int getIdBudget() {
		return this.idBudget;
	}

	public void setIdBudget(int idBudget) {
		this.idBudget = idBudget;
	}

	public int getMontantBudget() {
		return this.montantBudget;
	}

	public void setMontantBudget(int montantBudget) {
		this.montantBudget = montantBudget;
	}

	public String getNomBudget() {
		return this.nomBudget;
	}

	public void setNomBudget(String nomBudget) {
		this.nomBudget = nomBudget;
	}

	public String getTypeBudget() {
		return this.typeBudget;
	}

	public void setTypeBudget(String typeBudget) {
		this.typeBudget = typeBudget;
	}

	public Uniteoeuvre getUniteoeuvre() {
		return this.uniteoeuvre;
	}

	public void setUniteoeuvre(Uniteoeuvre uniteoeuvre) {
		this.uniteoeuvre = uniteoeuvre;
	}

	public Livrable getLivrable() {
		return this.livrable;
	}

	public void setLivrable(Livrable livrable) {
		this.livrable = livrable;
	}

	public Workpage getWorkpage() {
		return this.workpage;
	}

	public void setWorkpage(Workpage workpage) {
		this.workpage = workpage;
	}

}