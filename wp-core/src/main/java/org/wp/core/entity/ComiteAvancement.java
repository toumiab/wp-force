package org.wp.core.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the comite_avancement database table.
 * 
 */
@Entity
@Table(name="comite_avancement")
@NamedQuery(name="ComiteAvancement.findAll", query="SELECT c FROM ComiteAvancement c")
public class ComiteAvancement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="COMITE_AVANCEMENT_IDCOMITE_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.AUTO, generator="COMITE_AVANCEMENT_IDCOMITE_GENERATOR")
	@Column(name="id_comite")
	private int idComite;

	@Temporal(TemporalType.DATE)
	@Column(name="date_comite")
	private Date dateComite;

	@Column(name="fournisseurid_four" , insertable = false, updatable = false)
	private int fournisseuridFour;

	@Column(name="nom_comite")
	private String nomComite;

	@Column(name="sujet_comite")
	private String sujetComite;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="userId")
	private User user;
	
	//bi-directional many-to-one association to Uniteoeuvre
	@ManyToOne
	@JoinColumn(name="fournisseurid_four")
	private Fournisseur fournisseur;	

	public ComiteAvancement() {
	}

	public int getIdComite() {
		return this.idComite;
	}

	public void setIdComite(int idComite) {
		this.idComite = idComite;
	}

	public Date getDateComite() {
		return this.dateComite;
	}

	public void setDateComite(Date dateComite) {
		this.dateComite = dateComite;
	}

	public int getFournisseuridFour() {
		return this.fournisseuridFour;
	}

	public void setFournisseuridFour(int fournisseuridFour) {
		this.fournisseuridFour = fournisseuridFour;
	}

	public String getNomComite() {
		return this.nomComite;
	}

	public void setNomComite(String nomComite) {
		this.nomComite = nomComite;
	}

	public String getSujetComite() {
		return this.sujetComite;
	}

	public void setSujetComite(String sujetComite) {
		this.sujetComite = sujetComite;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}
	
	

}