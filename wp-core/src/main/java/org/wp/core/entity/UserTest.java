/**
 * 
 */
package org.wp.core.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Abdennasser
 *
 */
@Entity
public class UserTest {
	
	@Id
	private int idUser;
	
	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	

}
