package org.wp.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the groupe database table.
 * 
 */
@Entity
@NamedQuery(name="Groupe.findAll", query="SELECT g FROM Groupe g")
public class Groupe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="GROUPE_ID_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.AUTO, generator="GROUPE_ID_GENERATOR")
	private int id;

	private String description;

	@Column(name="group_name")
	private String groupName;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="id", referencedColumnName = "id", insertable = false, updatable = false)
	private User user;

	//bi-directional one-to-one association to Licence
	@OneToOne(mappedBy="groupe")
	private Licence licence;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="groupe")
	private List<User> users;

	public Groupe() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Licence getLicence() {
		return this.licence;
	}

	public void setLicence(Licence licence) {
		this.licence = licence;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setGroupe(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setGroupe(null);

		return user;
	}

}