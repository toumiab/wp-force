package org.wp.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the uniteoeuvre database table.
 * 
 */
@Entity
@NamedQuery(name="Uniteoeuvre.findAll", query="SELECT u FROM Uniteoeuvre u")
public class Uniteoeuvre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="UNITEOEUVRE_IDUOUVRE_GENERATOR")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="UNITEOEUVRE_IDUOUVRE_GENERATOR")
	@Column(name="id_uouvre")
	private int idUouvre;

	@Temporal(TemporalType.DATE)
	@Column(name="date_debut")
	private Date dateDebut;

	@Temporal(TemporalType.DATE)
	@Column(name="date_fin")
	private Date dateFin;

	//bi-directional many-to-one association to Livrable
	@ManyToOne
	@JoinColumn(name="livrableid_livrable")
	private Livrable livrable;

	//bi-directional many-to-one association to Budget
	@OneToMany(mappedBy="uniteoeuvre")
	private List<Budget> budgets;

	public Uniteoeuvre() {
	}

	public int getIdUouvre() {
		return this.idUouvre;
	}

	public void setIdUouvre(int idUouvre) {
		this.idUouvre = idUouvre;
	}

	public Date getDateDebut() {
		return this.dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return this.dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public Livrable getLivrable() {
		return this.livrable;
	}

	public void setLivrable(Livrable livrable) {
		this.livrable = livrable;
	}

	public List<Budget> getBudgets() {
		return this.budgets;
	}

	public void setBudgets(List<Budget> budgets) {
		this.budgets = budgets;
	}

	public Budget addBudget(Budget budget) {
		getBudgets().add(budget);
		budget.setUniteoeuvre(this);

		return budget;
	}

	public Budget removeBudget(Budget budget) {
		getBudgets().remove(budget);
		budget.setUniteoeuvre(null);

		return budget;
	}

}