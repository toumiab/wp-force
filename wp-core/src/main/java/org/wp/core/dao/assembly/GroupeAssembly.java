/**
 * 
 */
package org.wp.core.dao.assembly;

import org.wp.commun.representation.GroupeRepresentation;
import org.wp.core.entity.Groupe;

/**
 * @author atoumi
 *
 */
public class GroupeAssembly {
	
	public GroupeRepresentation getRepdoAssemblerDtoFromAggregate(Groupe groupe){
		GroupeRepresentation rep = new GroupeRepresentation();
		rep.setDescription(groupe.getDescription());
		if(null != groupe.getGroupName()) {
			rep.setGroupName(new String(groupe.getGroupName()));
		}
		rep.setIdGroup(groupe.getId());
//		if(null != groupe.getSuperGroup()){
//			rep.setSuperGroup(groupe.getSuperGroup());
//		}
		return rep;
	}
	
	public Groupe getDtoAssemblerRepFromAggregate(GroupeRepresentation rep) {
		Groupe groupe = new Groupe();
		groupe.setDescription(rep.getDescription());
		if(null != rep.getGroupName()) {
			groupe.setGroupName(rep.getGroupName());
		}
		groupe.setId(rep.getIdGroup());
	//	groupe.setSuperGroup(rep.getSuperGroup());
		return groupe;
	}

}
