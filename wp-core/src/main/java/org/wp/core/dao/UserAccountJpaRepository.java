/**
 * 
 */
package org.wp.core.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.wp.core.entity.User;

/**
 * @author Abdennasser
 *
 */
@Repository
public class UserAccountJpaRepository extends GenericDaoImpl<User> implements UserAccountRepository {

	/** The Constant SELECT_USER. */
	private static final String SELECT_USER = "from User userAcc  where  userAcc.email=:email and userAcc.passwordHash=:passwordHash ";

	/* (non-Javadoc)
	 * @see org.wp.core.dao.IGenericDao#create(java.io.Serializable)
	 */
	@Override
	public User create(User t) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.wp.core.dao.IGenericDao#delete(java.lang.Object)
	 */
	@Override
	public void delete(Object id) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.wp.core.dao.IGenericDao#find(java.lang.Object)
	 */
	
	public User find(String email, String password) {
		User user = null;
		try {
			TypedQuery<User> query = this.entityManager.createQuery(SELECT_USER, User.class);
			query.setParameter("email", email);
			query.setParameter("passwordHash", password);
			List<User> result = query.getResultList();
			if (!CollectionUtils.isEmpty(result)) {
				if (result.get(0) != null) {
					user = result.get(0);
				}
			}
		} catch (NoResultException nre) {
			// Ignore this because as per your logic this is ok!
			new NoResultException(nre.getMessage());
		}
		return user;
	}

	/* (non-Javadoc)
	 * @see org.wp.core.dao.IGenericDao#getAll()
	 */
	@Override
	public List<User> getAllUserAccount() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.wp.core.dao.IGenericDao#update(java.io.Serializable)
	 */
	@Override
	public User update(User t) {
		// TODO Auto-generated method stub
		return null;
	}

}
