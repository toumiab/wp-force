package org.wp.core.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

public abstract class GenericDaoImpl<T extends Serializable>  implements IGenericDao<T> {

	
	private static final Logger logger = Logger.getLogger(GenericDaoImpl.class);

	// parametrized type of a concrete class
	private Class<T> type;
	
	/**
	 * Represents a JPA connection to the object database, we can uses it for CRUD operations (for interacting with the
	 * persistence context)
	 */
	@PersistenceContext(unitName = "wpUnit")
	protected EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class<T>) pt.getActualTypeArguments()[0];
	}

	public long count() {
		String entity = type.getSimpleName();
		final StringBuilder queryString = new StringBuilder("select count(ent) from ".concat(entity).concat(" ent"));
		// create an instance of Query for executing a Java Persistence query language statement
		final Query query = entityManager.createQuery(queryString.toString());
		// execute a SELECT query that returns a single untyped result
		return (Long) query.getSingleResult();
	}

	public T find(final Object id) {
		return entityManager.find(type, id);
	}

	public T create(T t) {
		entityManager.persist(t);
		return t;
	}

	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		Query query = entityManager.createQuery("from ".concat(type.getName()));
		return query.getResultList();
	}

	public T update(T t) {
		return entityManager.merge(t);
	}

	public void delete(Object id) {
		entityManager.remove(entityManager.getReference(type, id));
	}	

}
