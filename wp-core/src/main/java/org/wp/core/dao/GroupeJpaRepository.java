package org.wp.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.wp.core.entity.Groupe;

@Repository
public class GroupeJpaRepository extends GenericDaoImpl<Groupe> implements GroupeRepository {

	public List<Groupe> getAllGroupe() {
		return this.getAll();
	}

}
