/**
 * 
 */
package org.wp.core.dao;

import java.util.List;

import org.wp.core.entity.User;

/**
 * @author Abdennasser
 *
 */
public interface UserAccountRepository extends IGenericDao<User> {
	List<User> getAllUserAccount();
	
	public User find(String email, String password);
}
