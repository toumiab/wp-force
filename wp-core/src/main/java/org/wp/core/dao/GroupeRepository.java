/**
 * 
 */
package org.wp.core.dao;

import java.util.List;

import org.wp.core.entity.Groupe;

/**
 * @author atoumi
 *
 */
public interface GroupeRepository extends IGenericDao<Groupe> {
	
	List<Groupe> getAllGroupe();

}
