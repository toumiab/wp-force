/**
 * 
 */
package org.wp.core.dao.assembly;

import org.wp.commun.representation.UserAccountRepresentation;
import org.wp.core.entity.User;

/**
 * @author Abdennasser
 *
 */
public class UserAccountAssembly {
	
	public UserAccountRepresentation getRepdoAssemblerDtoFromAggregate(User userAccount){
		UserAccountRepresentation rep = new UserAccountRepresentation();
		rep.setId(userAccount.getId());
		rep.setLockoutEnable(userAccount.getLockoutEnable());
		rep.setEmail(userAccount.getEmail());
		rep.setFirstName(userAccount.getFirstName());
		rep.setId(userAccount.getId());
		rep.setPasswordHash(userAccount.getPasswordHash());
		return rep;
	}

}
