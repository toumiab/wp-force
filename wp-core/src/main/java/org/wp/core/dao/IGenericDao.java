/**
 * 
 */
package org.wp.core.dao;

import java.io.Serializable;
import java.util.List;

/**
 * @author atoumi
 *
 */
public interface IGenericDao<T extends Serializable>  {
	/**
	 * Returns the total number of results
	 */
	public long count();

	/**
	 * Create and save new object in DB.
	 */
	public T create(T t);

	/**
	 * Remove the entity with the specified type and id from the datastore.
	 */
	public void delete(Object id);

	/**
	 * Get the entity with the specified type and id from the datastore.
	 * 
	 * @param id of the entity.
	 * @return If none is found, return null.
	 */
	public T find(Object id);

	/**
	 * Get a list of all the objects of the specified type.
	 */
	public List<T> getAll();

	/**
	 * Update object in DB.
	 */
	public T update(T t);
}
