/**
 * 
 */
package org.wp.core.services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wp.commun.representation.UserAccountRepresentation;
import org.wp.core.dao.UserAccountRepository;
import org.wp.core.dao.assembly.UserAccountAssembly;
import org.wp.core.entity.User;

/**
 * @author Abdennasser
 *
 */
@Service
public class UserServicesImpl implements UserServices {
	
	private UserAccountAssembly userAccountAssembly;
	
	@Autowired
	private UserAccountRepository userAccount;
	
	public UserServicesImpl(){
		this.userAccountAssembly = new UserAccountAssembly();
	}
	
	public UserAccountRepresentation findUser(String email, String password){
		UserAccountRepresentation userAccount = new UserAccountRepresentation();
		User user = this.userAccount.find(email, password);
		if(null != user){
			userAccount = this.userAccountAssembly.getRepdoAssemblerDtoFromAggregate(user);
		}else if(email.equals("root@yahoo.fr")){
			
		}
		return userAccount;
	}
	
	/**
	 * Méthode de calcul d'un hash pour un mot de passe et sel donnés
	 * 
	 * @param password
	 *            à être masqué
	 * @param salt
	 *            pour la sécurité
	 * @return hash
	 * @throws NoSuchAlgorithmException
	 */
	public static byte[] hash(final byte[] password, final byte[] salt)
			throws NoSuchAlgorithmException {
		MessageDigest md;
		md = MessageDigest.getInstance("SHA-1");
		final byte[] truncatedHash = new byte[64];
		md.update(salt);
		md.update(password);
		final byte[] sha1hash = md.digest();
		for (int i = 0; i < truncatedHash.length && i < sha1hash.length; i++) {
			truncatedHash[i] = sha1hash[i];
		}
		return truncatedHash;
	}

}
