/**
 * 
 */
package org.wp.core.services;

import org.wp.commun.representation.UserAccountRepresentation;

/**
 * @author Abdennasser
 *
 */
public interface UserServices {
	
	public UserAccountRepresentation findUser(String login, String mpass);

}
