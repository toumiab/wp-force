/**
 * 
 */
package org.wp.core.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.wp.commun.representation.GroupeRepresentation;
import org.wp.commun.representation.WPException;
import org.wp.core.dao.GroupeRepository;
import org.wp.core.dao.assembly.GroupeAssembly;
import org.wp.core.entity.Groupe;

/**
 * @author atoumi
 *
 */
@Service
public class GroupeServicesImpl implements GroupeServices {
	
	 private GroupeAssembly groupeAssembly;
	 
	 @Autowired
	 private GroupeRepository groupeRepo;
	 
	 public GroupeServicesImpl(){
		 this.groupeAssembly = new GroupeAssembly();
	 }

	public List<GroupeRepresentation> getAllGroupe() {
		List<GroupeRepresentation> groupeReps = new ArrayList<GroupeRepresentation>();
		List<Groupe> groupes = this.groupeRepo.getAll();
		for(Groupe gr: groupes){
			groupeReps.add(this.groupeAssembly.getRepdoAssemblerDtoFromAggregate(gr));
		}
		return groupeReps;
	}
	
	@Transactional
	public String createGroupe(GroupeRepresentation  rep) throws WPException{
		String create = "ok";
		Groupe grp = this.groupeAssembly.getDtoAssemblerRepFromAggregate(rep);
		this.groupeRepo.create(grp);
		if(null == grp) {
			throw new  WPException("La creéation de groupe a été echoué");
		}
		return create;
	}
	
	@Transactional
	public void updateGroupe(GroupeRepresentation rep) throws WPException{
		Groupe grp = this.groupeAssembly.getDtoAssemblerRepFromAggregate(rep);
		this.groupeRepo.update(grp);
	}
	
	@Transactional
	public void deleteGroupe(int idGroup) throws WPException{
		this.groupeRepo.delete(idGroup);
	}

}
