/**
 * 
 */
package org.wp.core.services;

import java.util.List;

import org.wp.commun.representation.GroupeRepresentation;
import org.wp.commun.representation.WPException;


/**
 * @author atoumi
 *
 */
public interface GroupeServices {
	
	public List<GroupeRepresentation> getAllGroupe();
	
	public String createGroupe(GroupeRepresentation  rep) throws WPException;
	
	public void updateGroupe(GroupeRepresentation rep) throws WPException;
	
	public void deleteGroupe(int idGroup) throws WPException;
 
 
}
